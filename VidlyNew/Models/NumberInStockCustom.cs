﻿using System.ComponentModel.DataAnnotations;

namespace VidlyNew.Models
{
    public class NumberInStockCustom : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Movie movie = (Movie)validationContext.ObjectInstance;
            if (movie.NumberInStock > Movie.MinInStock && movie.NumberInStock <= Movie.MaxInStock)
            {
                return ValidationResult.Success;
            }
            return new ValidationResult("Number in stock should be beetween 1 and 20");
        }
    }
}