﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VidlyNew.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }


        public Genre Genre { get; set; }

        [Required]
        [Display(Name = "Genre")]
        public byte GenreId { get; set; }

        [Required]
        [Display(Name = "Release Date")]
        public DateTime ReleaseDate { get; set; }

        [Required]
        public DateTime DateAdded { get; set; }


        [Display(Name = "Number in Stock")]
        [NumberInStockCustom]
        public int NumberInStock { get; set; }

        public static readonly byte MaxInStock = 20;
        public static readonly byte MinInStock = 1;
    }
}