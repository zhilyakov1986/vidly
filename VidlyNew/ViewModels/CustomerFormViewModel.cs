﻿using System.Collections.Generic;
using VidlyNew.Models;

namespace VidlyNew.ViewModels
{
    public class CustomerFormViewModel
    {
        public IEnumerable<MembershipType> MembershipTypes { get; set; }
        public Customer Customer { get; set; }
    }
}