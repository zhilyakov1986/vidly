﻿using System;
using System.ComponentModel.DataAnnotations;
using VidlyNew.Models;

namespace VidlyNew.Dtos
{
    public class MovieDto
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }


        public Genre Genre { get; set; }

        [Required]
        public byte GenreId { get; set; }

        [Required]
        public DateTime ReleaseDate { get; set; }

        [Required]
        public DateTime DateAdded { get; set; }


        //[NumberInStockCustom]
        public int NumberInStock { get; set; }
    }
}