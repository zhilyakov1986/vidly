namespace VidlyNew.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class populateNamesForMembershipType : DbMigration
    {
        public override void Up()
        {
            Sql("Update MembershipTypes Set Name='Pay as you go' where Id = 1");
            Sql("Update MembershipTypes Set Name='Monthly' where Id = 2");
            Sql("Update MembershipTypes Set Name='Quaterly' where Id = 3");
            Sql("Update MembershipTypes Set Name='Annually' where Id = 4");
        }

        public override void Down()
        {
        }
    }
}
