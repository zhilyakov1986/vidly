namespace VidlyNew.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c6e5d766-30bf-4c4d-8a25-637c47af3ec6', N'admin@vidly.com', 0, N'ABH15uRpUkcEGLp6OLwapwRzen+56UdAy8usEpONVYbOEPBFm6/pbe/jvlsT8qZd8g==', N'b840b8c0-734b-46eb-8935-0fb555e49d00', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'4e79874c-13b6-4984-bbee-966a00af7ee9', N'guest@vidly.com', 0, N'AHd0JHMj+fFfFe5SZvGuBLGZIiTlkRMuMABkm2UhLN9/dyDr6OOg9Ble3IdmbzaF4w==', N'1c0c7f5b-932c-4e73-a63f-e3753452aab6', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'828a5a16-0a41-4ee9-82b4-9f6d114b3ec4', N'CanManageMovies')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c6e5d766-30bf-4c4d-8a25-637c47af3ec6', N'828a5a16-0a41-4ee9-82b4-9f6d114b3ec4')

");
        }

        public override void Down()
        {
        }
    }
}
