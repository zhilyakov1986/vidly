namespace VidlyNew.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class populateGenre : DbMigration
    {
        public override void Up()
        {
            Sql("insert into Genres (Id, Name) values (1, 'Comedy')");
            Sql("insert into Genres (Id, Name) values (2, 'Action')");
            Sql("insert into Genres (Id, Name) values (3, 'Triller')");
            Sql("insert into Genres (Id, Name) values (4, 'Sci-Fi')");
            Sql("insert into Genres (Id, Name) values (5, 'Animation')");
        }

        public override void Down()
        {
        }
    }
}
